#!/bin/bash

ps -ef | grep timidity | grep -v grep
[ $?  -eq "0" ] && echo "timidity is running" || sudo service timidity start && sleep 0.25

ps -ef | grep lightorgan | grep -v grep
[ $?  -eq "0" ] && echo "lightorgan is running" || sudo ./lightorgan && sleep 0.25

#let's connect timidity and lightorgan
aconnect 14:0 128:0

aconnect -ol

echo "

*************

Now that it is running, play a midi like so:

aplaymidi -p 14 file.midi

"
