# Dancing Pi Lights

A simple controller to make lights turn on and off in time with MIDI music using a Rapsberry Pi and PiFace.

Requires WiringPi http://wiringpi.com/

Uses a modified version of Chivalry Timbers' lightorgan http://chivalrytimberz.wordpress.com/2012/12/03/pi-lights/
